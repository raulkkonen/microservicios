package com.springboot.api.jdbc.service;

import java.util.List;
import com.springboot.api.jdbc.model.Producto;

public interface ProductoService {
	
	List<Producto> getAllProductos();
	Producto getProducto(Integer id);
	void saveProducto(Producto producto);
	void deleteProducto(Integer id);		

}
