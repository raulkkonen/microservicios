package com.springboot.api.jdbc.dao;

import java.util.List;
import com.springboot.api.jdbc.model.Producto;

public interface ProductoDao {
	
	List<Producto> getAllProductos();
	Producto getProducto(Integer id);
	void saveProducto(Producto producto);
	void deleteProducto(Integer id);	

}
