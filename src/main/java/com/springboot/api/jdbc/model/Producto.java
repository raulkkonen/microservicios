package com.springboot.api.jdbc.model;

public class Producto {
	
	private Integer id;
	private String descripcion;
	private String categoria;
	private Double precio_unitario;
	private Long stock_actual;
	private Long stock_minimo;
	private String estado;		
	
	public Producto() {
		
	}	
	
	
	
	public Producto(Integer id, String descripcion, String categoria, Double precio_unitario, Long stock_actual,
			Long stock_minimo, String estado) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.categoria = categoria;
		this.precio_unitario = precio_unitario;
		this.stock_actual = stock_actual;
		this.stock_minimo = stock_minimo;
		this.estado = estado;
	}


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public Double getPrecio_unitario() {
		return precio_unitario;
	}
	public void setPrecio_unitario(Double precio_unitario) {
		this.precio_unitario = precio_unitario;
	}
	public Long getStock_actual() {
		return stock_actual;
	}
	public void setStock_actual(Long stock_actual) {
		this.stock_actual = stock_actual;
	}
	public Long getStock_minimo() {
		return stock_minimo;
	}
	public void setStock_minimo(Long stock_minimo) {
		this.stock_minimo = stock_minimo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}



	@Override
	public String toString() {
		return "Producto [id=" + id + ", descripcion=" + descripcion + ", categoria=" + categoria + ", precio_unitario="
				+ precio_unitario + ", stock_actual=" + stock_actual + ", stock_minimo=" + stock_minimo + ", estado="
				+ estado + "]";
	}

	

}

